package day1;


import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.db.Dbconnection;

public class InsertMany {

  public static void main(String[] args) {
      Connection connection = Dbconnection.getConnection();
      Statement statement = null;

      try {
          insertEmployeeDetails(connection);
      } catch (SQLException e) {
          e.printStackTrace();
      } finally {
          try {
              if (connection != null) {
                  if (statement != null) {
                      statement.close();
                  }
                  connection.close();
              }
          } catch (SQLException e) {
              e.printStackTrace();
          }
      }
  }

  private static void insertEmployeeDetails(Connection connection) throws SQLException {
      Scanner scanner = new Scanner(System.in);

      System.out.print("Enter Employee ID: ");
      int empId = scanner.nextInt();

      System.out.print("Enter Employee Name: ");
      String empName = scanner.next();

      System.out.print("Enter Salary: ");
      double salary = scanner.nextDouble();

      System.out.print("Enter Gender: ");
      String gender = scanner.next();

      System.out.print("Enter Email ID: ");
      String emailId = scanner.next();

      System.out.print("Enter Password: ");
      String password = scanner.next();

      String insertQuery = "insert into employee values (" +
              empId + ", '" + empName + "', " + salary + ", '" +
              gender + "', '" + emailId + "', '" + password + "')";

      try (Statement statement = connection.createStatement()) {
          int result = statement.executeUpdate(insertQuery);

          if (result > 0) {
              System.out.println(result + " Record(s) Inserted...");
          } else {
              System.out.println("Record Insertion Failed...");
          }
      }
  }
}